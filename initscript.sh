#!/bin/busybox sh

# Copyright (c) 2021 Valve Corporation
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice (including the next
# paragraph) shall be included in all copies or substantial portions of the
# Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Author: Martin Peres <martin.peres@mupuf.org>
#

set -eux

MODULES_PATH=/usr_mods
CONTAINER_MOUNTPOINT=/container
CONTAINER_ROOTFS="$CONTAINER_MOUNTPOINT/rootfs"
CONTAINER_CACHE="$CONTAINER_MOUNTPOINT/cache"
CACHE_PARTITION_LABEL="B2C_CACHE"

function log {
    { set +x; } 2>/dev/null
    echo -e "[$(busybox cut -d ' ' -f1 /proc/uptime)]: $*\n"
    set -x
}

function setup_busybox {
    for cmd in `busybox --list`; do
        [ -f "/bin/$cmd" ] || busybox ln -s /bin/busybox /bin/$cmd
    done
    log "Busybox setup: DONE"
}

function setup_mounts {
    mount -t proc none /proc
    mount -t sysfs none /sys
    mount -t devtmpfs none /dev
    mkdir -p /dev/pts
    mount -t devpts devpts /dev/pts

    # Mount cgroups
    mount -t cgroup2 none /sys/fs/cgroup
    cd /sys/fs/cgroup/
    for sys in $(awk '!/^#/ { if ($4 == 1) print $1 }' /proc/cgroups); do
        mkdir -p $sys
        if ! mount -n -t cgroup -o $sys cgroup $sys; then
            rmdir $sys || true
        fi
    done

    log "Mounts setup: DONE"
}

function setup_env {
    export HOME=/root
}

ARG_CACHE_DEVICE="none"
ARG_CONTAINER=""
ARG_MODULES=""
ARG_NTP_PEER="none"
ARG_PIPEFAIL="0"
ARG_POST_CONTAINER=""
ARG_SHUTDOWN_CMD="poweroff -f"
function parse_cmdline {
    cmdline=$(busybox cat /proc/cmdline)

    # Go through the list of options in the commandline, but remove the quotes
    # around multi-words arguments. Awk seems to be doing that best.
    OLDIFS=$IFS IFS=$'\n'
    for param in $(echo "$cmdline" | awk -F\" 'BEGIN { OFS = "" } {
        for (i = 1; i <= NF; i += 2) {
            gsub(/[ \t]+/, "\n", $i)
        }
        print
    }'); do
        value="${param#*=}"
        case $param in
            b2c.insmods=*)
                ARG_MODULES=$value
                ;;
            b2c.cache_device=*)
                ARG_CACHE_DEVICE=$value
                ;;
            b2c.container=*)
                ARG_CONTAINER="$ARG_CONTAINER$value\n"
                ;;
            b2c.post_container=*)
                ARG_POST_CONTAINER="$ARG_POST_CONTAINER$value\n"
                ;;
            b2c.pipefail)
                ARG_PIPEFAIL="1"
                ;;
            b2c.ntp_peer=*)
                ARG_NTP_PEER=$value
                ;;
            b2c.shutdown_cmd=*)
                ARG_SHUTDOWN_CMD="$value"
                ;;
        esac
    done
    IFS=$OLDIFS
    # TODO: add a parameter to download a volume with firmwares and modules
}

function load_modules {
    [ -z "$@" ] && return

    for mod_name in `echo "$@" | busybox tr ',' '\n'`; do
        path="$MODULES_PATH/$mod_name"
        echo "Load the module: $path"
        insmod "$path"
    done

    log "Loading requested modules: DONE"
}

function connect {
    ip link set eth0 up || return 1
    udhcpc -i eth0 -s /etc/uhdcp-default.sh -T 1 || return 1
    log "Getting IP: DONE"
}

function ntp_set {
    case $1 in
        none)
            log "WARNING: Did not reset the time, use b2c.ntp_peer=auto to set it on boot"
            return 0
            ;;
        auto)
            peer_addr="pool.ntp.org"
            ;;
        *)
            peer_addr=$1
    esac

    # Limit the maximum execution time to prevent the boot sequence to be stuck
    # for too long
    status="DONE"
    time timeout 5 ntpd -dnq -p "$peer_addr" || status="FAILED"

    log "Getting the time from the NTP server $peer_addr: $status"
}

function find_container_partition {
    dev_name=`blkid | grep "LABEL=\"$CACHE_PARTITION_LABEL\"" | head -n 1 | cut -d ':' -f 1`
    if [ -n "$dev_name" ]; then
        echo $dev_name
        return 0
    else
        return 1
    fi
}

function format_cache_partition {
    log "Formating the partition $CONTAINER_PART_DEV"
    mkfs.ext4 -F -L "$CACHE_PARTITION_LABEL" "$CONTAINER_PART_DEV"
}

function format_disk {
    if [ -n "$1" ]; then
        parted --script $1 mklabel gpt
        parted --script $1 mkpart primary ext4 2048s 100%

        CONTAINER_PART_DEV=`lsblk -no PATH $1 | tail -n -1`
        format_cache_partition

        return $?
    fi

    return 1
}

function find_or_create_cache_partition {
    # See if we have an existing block device that would work
    CONTAINER_PART_DEV=`find_container_partition` && return 0

    # Find a suitable disk
    sr_disks_majors=`grep ' sr' /proc/devices | sed "s/^[ \t]*//" | cut -d ' ' -f 1 | tr '\n' ',' | sed 's/,$//'`
    disk=`lsblk -ndlfp -e "$sr_disks_majors" | head -n 1`

    log "No existing cache partition found on this machine, create one from the disk $disk"

    # Find a disk, partition it, then format it as ext4
    format_disk $disk || return 1

    return 0
}

function try_to_use_cache_device {
    # Check if the parameter is a path to a file
    if [ -f "$ARG_CACHE_DEVICE" ]; then
        log "The caching parameter '$ARG_CACHE_DEVICE' is neither 'none', 'auto', or a path to a block device. Defaulting to 'none'"
        return 0
    fi

    # $ARG_CACHE_DEVICE has to be a path to a drive
    # NOTE: Pay attention to the space after $ARG_CACHE_DEVICE, as it
    # makes sure that we don't accidentally match /dev/sda1 when asking
    # for /dev/sda.
    blk_dev=`lsblk -rpno PATH,TYPE,LABEL | grep "$ARG_CACHE_DEVICE "`
    if [ -z "$blk_dev" ]; then
        log "Error: The device '$ARG_CACHE_DEVICE' is neither a block device, nor a partition. Defaulting to no caching."
        return 1
    fi

    path=$(echo "$blk_dev" | cut -d ' ' -f 1)
    type=$(echo "$blk_dev" | cut -d ' ' -f 2)
    label=$(echo "$blk_dev" | cut -d ' ' -f 3)
    case $type in
        part)
            CONTAINER_PART_DEV="$path"
            if [ -z "$label" ]; then
                format_cache_partition
                return $?
            fi
            ;;

        disk)
            # Look for the first partition from the drive $1, that has the right cache
            CONTAINER_PART_DEV=`lsblk -no PATH,LABEL $path | grep "$CACHE_PARTITION_LABEL" | cut -d ' ' -f 1 | head -n 1`
            if [ -n "$CONTAINER_PART_DEV" ]; then
                return 0
            else
                log "No existing cache partition on the drive $path, recreate the partition table and format a partition"
                format_disk $path
                return $?
            fi
            ;;
    esac

    return 0
}

CONTAINER_PART_DEV=""
function mount_cache_partition {
    [ -d "$CONTAINER_MOUNTPOINT" ] || mkdir "$CONTAINER_MOUNTPOINT"

    # Find a suitable cache partition
    case $ARG_CACHE_DEVICE in
        none)
            log "Do not use a partition cache"
            return 0
            ;;
        auto)
            find_or_create_cache_partition || return 0
            ;;
        *)
            try_to_use_cache_device "$ARG_CACHE_DEVICE" || return 0
            ;;
    esac

    log "Selected the partition $CONTAINER_PART_DEV as a cache"

    status="DONE"
    mount "$CONTAINER_PART_DEV" "$CONTAINER_MOUNTPOINT" || status="FAILED"
    log "Mounting the partition $CONTAINER_PART_DEV to $CONTAINER_MOUNTPOINT: $status"

    return 0
}

function setup_container_runtime {
    # HACK: I could not find a way to change the right parameter in podman's
    # config, so make a symlink for now
    [ -d "$CONTAINER_CACHE" ] || mkdir "$CONTAINER_CACHE"
    [ -f "/var/tmp" ] || ln -s "$CONTAINER_CACHE" /var/tmp

    # Squash a kernel warning
    echo 1 > /sys/fs/cgroup/memory/memory.use_hierarchy

    # Set some configuration files
    touch /etc/hosts
    echo "root:x:0:0:root:/root:/bin/sh" > /etc/passwd
    echo "containers:165536:65537" > /etc/subuid
    echo "containers:165536:65537" > /etc/subgid

    log "Container runtime setup: DONE"
}

function create_container {
    log "Pull, create, and init the container"

    # Podman is not super good at explaining what went wrong when creating a
    # container, so just try multiple times, each time increasing the size of
    # the hammer!
    for i in 0 1 2 3 4; do
        cmdline="podman create --rm --privileged --pull=always --network=host \
--runtime /bin/crun-no-pivot -e B2C_PIPELINE_STATUS=$B2C_PIPELINE_STATUS \
-e B2C_PIPELINE_FAILED_BY=\"$B2C_PIPELINE_FAILED_BY\" \
-e B2C_PIPELINE_PREV_CONTAINER=\"$B2C_PIPELINE_PREV_CONTAINER\" \
-e B2C_PIPELINE_PREV_CONTAINER_EXIT_CODE=$B2C_PIPELINE_PREV_CONTAINER_EXIT_CODE"

        # Set up the wanted container
        container_id=`eval "$cmdline $@"` && podman init "$container_id" && return 0

        # The command failed... Ignore the first 3 times, as we want to check it
        # is not a shortlived-network error
        if [ $i -eq 3 ]; then
            # Try resetting the entire state of podman, before trying again!
            podman system reset -f
        fi

        sleep 1
    done

    return 1
}

function start_container {
    container_id=""
    create_container $@ || return 1

    # Make sure that the layers got pushed to the drive before running the
    # container
    sync

    # HACK: Figure out how to use "podman wait" to wait for the container to be
    # ready for execution. Without this sleep, we sometimes fail to attach the
    # stdout/err to the container. Even a one ms sleep is sufficient in my
    # testing, but let's add a bit more just to be sure
    sleep .1

    log "About to start executing a container"
    exit_code=0
    podman start -a "$container_id" || exit_code=$?

    # Store the results of the execution
    B2C_PIPELINE_PREV_CONTAINER="$@"
    B2C_PIPELINE_PREV_CONTAINER_EXIT_CODE="$exit_code"

    return $exit_code
}

function start_containers {
    OLDIFS=$IFS IFS=$'\n'
    for container_params in $(echo -e "$@"); do
        exit_code=0
        start_container "$container_params" || exit_code=$?

        if [ $exit_code -eq 0 ] ; then
            log "The container run successfully, load the next one!"
        else
            # If this is the first container that failed, store that information
            if [ $B2C_PIPELINE_STATUS -eq 0 ]; then
                B2C_PIPELINE_STATUS="$exit_code"
                B2C_PIPELINE_FAILED_BY="$container_params"
            fi

            if [ $ARG_PIPEFAIL -eq 1 ]; then
                log "The container exited with error code $exit_code, aborting the pipeline..."
                return 0
            else
                log "The container exited with error code $exit_code, continuing..."
            fi
        fi
    done
    IFS=$OLDIFS

    return 0
}

function start_post_containers {
    log "Running the post containers"

    OLDIFS=$IFS IFS=$'\n'
    for container_params in $(echo -e "$@"); do
        start_container "$container_params" || /bin/true
    done
    IFS=$OLDIFS

    return 0
}

function container_cleanup {
    # HACK: podman has a nice race condition, so let's give a bit of time to
    # settle down... Sleeping isn't a problem here, since we are done testing :)
    sleep 1

    # Stop and delete all the containers that may still be running.
    # This should be a noop, but I would rather be safe than sorry :)
    podman container stop -a
    podman umount -a -f
    podman container rm -fa

    # Remove all the dangling images
    podman image prune -f
}

function run_containers {
    B2C_PIPELINE_STATUS="0"
    B2C_PIPELINE_FAILED_BY=""
    B2C_PIPELINE_PREV_CONTAINER=""
    B2C_PIPELINE_PREV_CONTAINER_EXIT_CODE=""

    if [ -n "$ARG_CONTAINER$ARG_POST_CONTAINER" ]; then
        setup_container_runtime
        start_containers "$ARG_CONTAINER"
        start_post_containers "$ARG_POST_CONTAINER"
        container_cleanup
    fi
}

# Do not print all the early commands
set +x

# Initial setup
setup_busybox
#setup_mounts  # To be continued to so we could boot without any go commands
setup_env

# Parse the kernel command line, in search of the b2c parameters
parse_cmdline

# Now that the early boot is over, let's log every command executed
set -x

# Load the user-requested modules
load_modules $ARG_MODULES

# Mount the cache partition
mount_cache_partition

# Connect to the network, now that the modules are loaded
if connect; then
    # Set the time
    ntp_set $ARG_NTP_PEER

    # Start the containers
    run_containers
else
    log "ERROR: Could not connect to the network, shutting down!"
fi

# Shutdown command
sync
eval $ARG_SHUTDOWN_CMD
