# Copyright (c) 2021 Valve Corporation
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice (including the next
# paragraph) shall be included in all copies or substantial portions of the
# Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Author: Martin Peres <martin.peres@mupuf.org>
#

FROM alpine:3.13

COPY patches /patches

RUN set -ex \
        && apk add --no-cache git bash go crun busybox ca-certificates e2fsprogs parted gpgme util-linux pigz \
        && apk add --no-cache -t build_deps upx make linux-headers gpgme-dev util-linux patch \
        && apk -X http://dl-cdn.alpinelinux.org/alpine/edge/main --no-cache add libseccomp-dev \
        && apk -X http://dl-cdn.alpinelinux.org/alpine/edge/community --no-cache add conmon \
        && git clone https://github.com/containers/podman.git /src/podman/ \
        && cd /src/podman \
        && patch -p1 < /patches/podman/0001-create-run-mirror-the-tls-verify-argument-from-pull.patch \
        && make EXTRA_LDFLAGS="-w -s" BUILD_ARG="exclude_graphdriver_btrfs btrfs_noversion exclude_graphdriver_devicemapper seccomp" podman \
        && upx --best bin/podman \
        && cp bin/podman /bin/podman \
        && rm -rf /src/podman /root/.cache \
        && apk del build_deps bash libseccomp-dev

# Don't ask me why it cannot be folded on the layer above!
RUN set -ex \
        && go get github.com/u-root/u-root

COPY config/containers uhdcp-default.sh /etc/

COPY crun-no-pivot initscript.sh /bin/

# TODO: By calling the initscript directly, we could save quite a bit of
# size, but we first need to study what the init program does for us :)
#-initcmd="/bin/initscript.sh" -nocmd
ENTRYPOINT /root/go/bin/u-root -files /bin/sh -defaultsh="" \
    -files /bin/crun-no-pivot -files /usr/bin/conmon \
    -files /usr/lib/libgpgme.so.11 \
    -files /bin/podman \
    -files /etc/containers \
    -files /usr/bin/unpigz -files /usr/bin/crun:bin/crun \
    -files /sbin/mkfs.ext4 -files /usr/sbin/parted:sbin/parted -files /bin/lsblk \
    -files /etc/ssl/certs/ca-certificates.crt \
    -files /usr_mods -files /etc/uhdcp-default.sh \
    -files /bin/initscript.sh \
    -uinitcmd="/bin/initscript.sh" github.com/u-root/u-root/cmds/core/init
